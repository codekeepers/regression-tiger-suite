package de.codekeepers.regressiontiger.dbtest;

import de.codekeepers.checkerberry.db.core.CheckerberryDb;
import de.codekeepers.checkerberry.db.core.CheckerberryDbEnvironment;
import de.codekeepers.checkerberry.junit5.connector.MethodNameDeterminationJunit5;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(MethodNameDeterminationJunit5.class)
public abstract class AbstractDbTest {
    private CheckerberryDbEnvironment dbEnvironment;
    private final Logger log = LoggerFactory.getLogger(AbstractDbTest.class);

    @BeforeEach
    public void setUpDbEnvironment() {
        log.info("Setup started");
        dbEnvironment = CheckerberryDb.getInstance().getEnvironment(null,
                new RtDescriptionCallback(), new RtDbConfigurationCallback());
        dbEnvironment.setUp(this);
    }

    public CheckerberryDbEnvironment getDbEnvironment() {
        return dbEnvironment;
    }

    @AfterAll
    public void tearDown() {
        if (dbEnvironment != null) {
            dbEnvironment.tearDown();
            dbEnvironment = null;
        }
    }
}
