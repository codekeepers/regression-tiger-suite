package de.codekeepers.regressiontiger.dbtest;

import de.codekeepers.checkerberry.db.bridge.configuration.DbConfiguration;
import de.codekeepers.checkerberry.db.bridge.configuration.DbConfigurationCallback;
import de.codekeepers.regressiontiger.config.TestConfiguration;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.ext.h2.H2DataTypeFactory;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.ext.postgresql.PostgresqlDataTypeFactory;

public class RtDbConfigurationCallback implements DbConfigurationCallback {
    @Override
    public void configure(DbConfiguration configuration) {
        // Datenbank-Konfiguration aus gui-test.*.properties laden
        TestConfiguration configurationManager = TestConfiguration.getInstance();

        // DTD konfigurieren
        configuration.setDatabaseDtd("-//Codekeepers//DTD jobdog-db 1.0//EN", "de/codekeepers/regressiontiger/jobdog-db.dtd");

        // JDBC konfigurieren
        configuration.setJdbcDriverClassName(configurationManager.dbJDBCDriver());
        configuration.setJdbcUrl(configurationManager.dbConnectionUrl());
        configuration.setJdbcUserName(configurationManager.dbUsername());
        configuration.setJdbcPassword(configurationManager.dbPassword());

        if (configurationManager.dbJDBCDriver().toLowerCase().contains("mysql")) {
            configuration.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());
        } else if (configurationManager.dbJDBCDriver().toLowerCase().contains("postgresql")) {
            configuration.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new PostgresqlDataTypeFactory());
            configuration.setProperty(DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES, true);
            configuration.setProperty(DatabaseConfig.PROPERTY_ESCAPE_PATTERN , "\"?\"");
        } else if (configurationManager.dbJDBCDriver().toLowerCase().contains("h2")) {
            configuration.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new H2DataTypeFactory());
        }
        configuration.setProperty(DatabaseConfig.FEATURE_ALLOW_EMPTY_FIELDS, true);

        // Tabellen konfigurieren
        configuration.setDumpFileEncoding("utf-8");

        // Allowed Tables: commented out
        configuration.addTablesToIgnoreList(
                /*"JOB",*/
        );
    }
}
