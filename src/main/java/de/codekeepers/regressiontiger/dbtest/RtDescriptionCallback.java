package de.codekeepers.regressiontiger.dbtest;

import de.codekeepers.checkerberry.db.bridge.context.DatabaseDescription;
import de.codekeepers.checkerberry.db.bridge.context.DatabaseDescriptionCallback;

public class RtDescriptionCallback implements DatabaseDescriptionCallback {

    @Override
    public void createDatabaseDescription(DatabaseDescription description) {
        description.addTableDescription("JOB", "DESCRIPTION")
                .addExcludedColumns("ID", "PUBLISHED_TIMESTAMP");
    }
}
