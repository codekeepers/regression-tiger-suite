package de.codekeepers.regressiontiger.config;

import org.aeonbits.owner.ConfigCache;


public class TestConfiguration {
    private static final TestConfiguration INSTANCE = new TestConfiguration();
    private final GuiTestProperties guiTestProperties;

    public static TestConfiguration getInstance() { return INSTANCE; }

    private TestConfiguration() {
        guiTestProperties = ConfigCache.getOrCreate(GuiTestProperties.class);
    }

    public String dbJDBCDriver() { return guiTestProperties.dbJDBCDriver(); }

    public String dbConnectionUrl() {
        return guiTestProperties.dbConnectionUrl();
    }

    public String dbUsername() {
        return guiTestProperties.dbUsername();
    }

    public String dbPassword() {
        return guiTestProperties.dbPassword();
    }

    public String testAppUrl() {
        return guiTestProperties.testAppUrl();
    }

    public String mountebankUrl() {
        return guiTestProperties.mountebankUrl();
    }
}
