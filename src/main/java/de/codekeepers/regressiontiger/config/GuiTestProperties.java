package de.codekeepers.regressiontiger.config;

import org.aeonbits.owner.Config;

/**
 *  Expected properties in file src/main/resources/gui-test.ci.properties:
 *      rt.db.jdbc-driver=...
 *      rt.db.connection-url=...
 *      rt.db.username=...
 *      rt.db.password=...
 *  Using the FIRST loading policy, the first properties file found takes precedence
 *  over files defined later. Default values can be provided with @DefaultValue.
 *  These have the lowest precedence.
 */
@Config.LoadPolicy(Config.LoadType.FIRST)
@Config.Sources({
        "classpath:gui-test.rwe.properties",
        "classpath:gui-test.afr.properties",
        "classpath:gui-test.ci.properties"})
public interface GuiTestProperties extends Config {
    @Key("rt.db.jdbc-driver")
    String dbJDBCDriver();

    @Key("rt.db.connection-url")
    String dbConnectionUrl();

    @Key("rt.db.username")
    String dbUsername();

    @Key("rt.db.password")
    String dbPassword();

    @Key("rt.testapp.url")
    String testAppUrl();

    @Key("rt.mountebank.url")
    String mountebankUrl();
}
