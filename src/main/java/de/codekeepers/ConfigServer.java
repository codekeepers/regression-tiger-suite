package de.codekeepers;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class ConfigServer {
    private static final Logger log = LoggerFactory.getLogger(ConfigServer.class);
    private final String url;
    private final HttpClient client;

    public ConfigServer(String url) {
        this.url = url + (url.endsWith("/") ? "" : "/");
        this.client = HttpClient.newHttpClient();
    }

    public String get(String key) {
        String value = "";
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url + key))
                .GET()
                .build();

        HttpResponse<String> response;
        try {
            response = client.send(request, HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() != 200) {
                throw new RuntimeException();
            }
            value = response.body();
        } catch (IOException | InterruptedException | RuntimeException e) {
            log.error("Failed to retrieve value from config server using endpoint '{}'", url + key);
        }

        return value;
    }

    public void set(String key, String value) {
        JSONObject body = new JSONObject();
        body.put("key", key);
        body.put("value", value);

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .POST(HttpRequest.BodyPublishers.ofString(body.toString()))
                .header("Content-Type", "application/json")
                .build();

        HttpResponse<String> response;
        try {
            response = client.send(request, HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() != 204) {
                throw new RuntimeException();
            }
        } catch (IOException | InterruptedException | RuntimeException e) {
            log.error("Failed to set value '{}' for key '{}' on config server '{}'", value, key, url);
        }
    }
}
