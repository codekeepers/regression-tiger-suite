package de.codekeepers.regressiontiger;

import de.codekeepers.ConfigServer;

import de.codekeepers.checkerberry.db.core.test.ClearTables;
import de.codekeepers.checkerberry.db.core.test.DbTestHandler;
import de.codekeepers.mounteclient.MounteClient;
import de.codekeepers.mounteclient.entities.*;

import de.codekeepers.regressiontiger.config.TestConfiguration;
import de.codekeepers.regressiontiger.dbtest.AbstractDbTest;
import org.json.JSONObject;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;

import static org.assertj.core.api.Assertions.assertThat;

public class T001postJobTest extends AbstractDbTest {
    private static final String JOBDOG_URL = TestConfiguration.getInstance().testAppUrl();
    private static final String CONFIG_SERVER_ENDPOINT = "https://xo94oqzj8e.execute-api.eu-central-1.amazonaws.com/dev/config";
    private static final String CONFIG_SERVER_KEY = "jobdog.publishservice.url";
    private static final String MOUNTEBANK_ENDPOINT = TestConfiguration.getInstance().mountebankUrl();
    private static final String MOUNTEBANK_IMPOSTER_ENDPOINT = "http://localhost:4545/dev/jobs";
    private static final ChromeOptions chromeOptions = new ChromeOptions();
    private static ConfigServer configServer;
    private static String originalEndpoint;
    private static MounteClient mounteClient;
    private static Imposter imposter;
    private WebDriver driver;
    private DbTestHandler dbTestHandler;

    private static String removePathFromURL(String urlString) {
        try {
            URL url = new URL(urlString);
            return new URL(url.getProtocol(), url.getHost(), url.getPort(), "").toString();
        } catch (MalformedURLException e) {
            throw new RuntimeException(String.format("Failed to remove path from URL '%s'", urlString), e);
        }
    }

    @BeforeAll
    public static void setUpClass() {
        chromeOptions.addArguments("--no-sandbox");
        chromeOptions.addArguments("--headless");

        mounteClient = new MounteClient(MOUNTEBANK_ENDPOINT);

        // switch URL on config server to imposter endpoint
        configServer = new ConfigServer(CONFIG_SERVER_ENDPOINT);
        originalEndpoint = configServer.get(CONFIG_SERVER_KEY);
        configServer.set(CONFIG_SERVER_KEY, MOUNTEBANK_IMPOSTER_ENDPOINT);

        // configure mountebank imposter
        PredicateGenerator predicateGen = PredicateGenerator.create()
                .matchMethod().matchPath().matchBody();
        ProxyResponse response = ProxyResponse.create()
                .withMode(ProxyMode.ONCE)
                .to(removePathFromURL(originalEndpoint))
                .addPredicateGenerator(predicateGen);
        Stub stub = Stub.create()
                .addResponse(response);
        imposter = Imposter.create()
                .withPort(4545)
                .withProtocol("http")
                .withName("Publish Service")
                .enableRequestRecording()
                .addStub(stub);
    }

    @BeforeEach
    public void setUp() {
        mounteClient.registerImposter(imposter);
        dbTestHandler = getDbEnvironment().getTestHandler();

        driver = new ChromeDriver(chromeOptions);
        driver.get(JOBDOG_URL);
    }

    @AfterEach
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
        mounteClient.removeImposter(imposter);
    }

    @AfterAll
    public static void tearDownClass() {
        configServer.set(CONFIG_SERVER_KEY, originalEndpoint);
    }

    @Test
    @ClearTables
    public void storeNewJob() throws InterruptedException, IOException, SQLException {
        driver.findElement(By.linkText("New Job")).click();
        driver.findElement(By.id("title")).click();
        driver.findElement(By.id("title")).sendKeys("Senior Java Dev");
        driver.findElement(By.id("description")).sendKeys("Cool Company");
        driver.findElement(By.id("btnPostJob")).click();

        // wait for publishing
        Thread.sleep(5000);

        // verify request
        JSONObject request = mounteClient.getRecordedRequests(imposter).getJSONObject(0);
        assertThat(request.get("method")).isEqualTo("POST");
        assertThat(request.get("path")).isEqualTo("/dev/jobs");
        assertThat(request.get("body")).isEqualTo("<job><title>Senior Java Dev</title><description>Cool Company</description></job>");

        // verify that job is in database
        dbTestHandler.createDiffReport("./diff.html");
        dbTestHandler.assertEqualsExpected();
    }

    @Test
    public void deleteJob() throws IOException, SQLException {
        DbTestHandler testHandler = getDbEnvironment().getTestHandler();

        driver.findElement(By.linkText("Delete")).click();

        testHandler.createDiffReport("./diff.html");
        testHandler.assertEqualsExpected();
    }
}
