package de.codekeepers.regressiontiger.dbtest;

import de.codekeepers.checkerberry.db.core.test.DbTestHandler;
import de.codekeepers.regressiontiger.dbtest.AbstractDbTest;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;

public class CreateDtdTest extends AbstractDbTest {

    @Test
    @Disabled
    public void createNewDtd() throws SQLException {
        DbTestHandler testHandler = getDbEnvironment().getTestHandler();
        String path = getClass().getName();
        path = path.replace('.', '/');
        testHandler.createCleanDtd("./src/test/resources/" + path + "-db" + ".dtd");
    }
}

