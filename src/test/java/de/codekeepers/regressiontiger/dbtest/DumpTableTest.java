package de.codekeepers.regressiontiger.dbtest;

import de.codekeepers.checkerberry.db.core.test.ClearTables;
import de.codekeepers.checkerberry.db.core.test.DbTestHandler;
import de.codekeepers.regressiontiger.config.TestConfiguration;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.sql.SQLException;

public class DumpTableTest extends AbstractDbTest {

    private WebDriver driver;

    @BeforeEach
    public void setUp() {
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--no-sandbox");
        chromeOptions.addArguments("--headless");
        driver = new ChromeDriver(chromeOptions);
        driver.get(TestConfiguration.getInstance().testAppUrl());
    }

    @AfterEach
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }

    @Test
    @Disabled
    @ClearTables
    public void dumpTable() throws SQLException, InterruptedException {
        driver.findElement(By.linkText("New Job")).click();
        driver.findElement(By.id("title")).click();
        driver.findElement(By.id("title")).sendKeys("Junior Consultant");
        driver.findElement(By.id("description")).sendKeys("Startup Company");
        driver.findElement(By.id("btnPostJob")).click();

        DbTestHandler testHandler = getDbEnvironment().getTestHandler();
        String path = getClass().getName();
        path = path.replace('.', '/');

        Thread.sleep(5000);

        testHandler.dumpTables("./src/test/resources/" + path + "_dumpTable_result.xml", "JOB");

    }
}

